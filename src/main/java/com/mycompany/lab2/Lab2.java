/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {
       static char[][]Table={{'7','8','9'},{'4','5','6'},{'1','2','3'}};
       static char[][]Tabletest={{'7','8','9'},{'4','5','6'},{'1','2','3'}};
       static char currentPlayer ='X';
       static char number;
     
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    static void printTable(){
       for(int i = 0; i < 3;i++ ){
            for(int j = 0; j < 3;j++){
                System.out.print(" "+Table[i][j]);
            }
                System.out.println(); 
            }
    }
    
    static void printTurn(){
        System.out.println(currentPlayer +" Turn"); 
        
    }
    static void inputNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.print("please input number: ");
        number = sc.next().charAt(0);
          
    }
    static boolean checkInput() {   
           for(int i = 0; i < 3;i++ ){
            for(int j = 0; j < 3;j++){
                if((Table[i][j])==Tabletest[i][j]){
                        if((Table[i][j])==number){
                            Table[i][j]=currentPlayer;
                            return true;
                         }       
                }
            } 
        }
          return false;
    }
    
    
    static void changePlayer() {
        
            if(currentPlayer=='X'){
                currentPlayer ='O';
            }else{
                currentPlayer ='X';
            }
        
    }
    
    static boolean checkRow() {   
            for(int i = 0; i < 3;i++){
                if((Table[i][0])==currentPlayer &&(Table[i][1])==currentPlayer &&(Table[i][2])==currentPlayer  ){
                    return true;
                    }     
            } 
            return false;
    }
    
    static boolean checkCol() {   
            for(int i = 0; i < 3;i++){
                if((Table[0][i])==currentPlayer &&(Table[1][i])==currentPlayer &&(Table[2][i])==currentPlayer  ){
                    return true;
                    }     
            } 
            return false;
    }
    
     static boolean checkX2() {   
            for(int i = 0; i < 3;i++){
                if((Table[i][2-i])!=currentPlayer  ){
                    return false;
                    }     
            } 
            return true;
    }
     
     static boolean checkX1() {   
            for(int i = 0; i < 3;i++){
            if((Table[i][i])!=currentPlayer ){
                return false;
                }     
            }
            return true;
    }
    
    static void printWin(){
          System.out.println(currentPlayer+" Win");
    }

    static boolean isWin(){
      
        if(checkRow()){
            return true;
        }if(checkCol()){
            return true;
        }if(checkX1()){
            return true;
        }if(checkX2()){
            return true;
        }
        return false;
    }
    
      static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[i][j] != 'X' && Table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;
      }
      
         static void printDraw(){
            System.out.println("Draw");
    }
    
    static String  inputContinue(String Continue) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to continue (yes or no) : ");
        Continue = sc.nextLine();
        reGame() ;
        return Continue;
    }

     static void reGame() {
        Table = Tabletest;
        currentPlayer = 'X';
    }

    public static void main(String[] args) { 
        
        String Continue ="yes";
        printWelcome();
        while(Continue.equals("yes")){
            printTable(); 
            printTurn();
            inputNumber(); 
            if(checkInput()){
                if(isWin()){
                    printTable();
                    printWin();
                    Continue=inputContinue( Continue);
                }else if (isDraw()) {
                    printTable();
                    printDraw();
                    Continue=inputContinue( Continue);
                }else{
                    changePlayer();}
            }
            
        }
           
    }
}



